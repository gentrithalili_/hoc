// import React from "react";

export function withAdminPermission(Component) {
  const user = JSON.parse(localStorage.getItem("hoc_user"));
  console.log(user);

  return function () {
    return user && user.role === "admin" ? <Component /> : null;
  };
}
