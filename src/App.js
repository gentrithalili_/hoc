import "./App.css";
import Message from "./Message";
import { withAdminPermission } from "./hoc/withAdminPermission";

function App() {
  const setUser = () => {
    localStorage.setItem(
      "hoc_user",
      JSON.stringify({ name: "hoc", role: "admin" })
    );
  };

  const getUser = () => {
    withAdminPermission();
  };

  return (
    <div className="App">
      <button onClick={setUser}>Set user</button>
      <button onClick={getUser}>Get user</button>
      <Message />
    </div>
  );
}

export default App;
