import { withAdminPermission } from "./hoc/withAdminPermission";

function Message() {
  return <h1>Message only for admin</h1>;
}

export default withAdminPermission(Message);
